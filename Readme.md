# Farzeen's Dotcastle

### Notes:

* Environment variables are included in ~/.profile. 
  If in future, ~/.bash_login or ~/.bash_profile needs to be created,
  either source ~/.profile inside those or include environment variables in those too.
  Because, ~/.profile won't be sourced by bash if either of the above file is found.
  LightDM is expected to source ~/.profile, regardless of the login shell.
