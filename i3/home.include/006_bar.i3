bar {
	font pango:Fira Mono Medium, Icons 8
    position bottom
    status_command i3status-rs $HOME/.config/i3/i3status-rust-config.toml
	colors {
		separator #666666
		background #222222
        statusline #dddddd
		focused_workspace #0088CC #0088CC #ffffff
        active_workspace #333333 #333333 #ffffff
		inactive_workspace #333333 #333333 #888888
		urgent_workspace #2f343a #900000 #ffffff
    }
}
