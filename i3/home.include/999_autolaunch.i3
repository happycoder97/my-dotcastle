exec --no-startup-id lxsession
exec --no-startup-id nm-applet
exec --no-startup-id blueman-applet
exec --no-startup-id nitrogen --restore
exec --no-startup-id syncthing-gtk
exec --no-startup-id keepassxc
exec --no-startup-id compton
exec --no-startup-id redshift -l '11.3214604:75.9340919'

