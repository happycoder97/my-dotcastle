

set $file_explorer	$FILE_EXPLORER
set $web_browser	$BROWSER
set $terminal		$TERMINAL

bindsym $mod+e		exec $file_explorer
bindsym $mod+w		exec $web_browser
bindsym $mod+Return	exec --no-startup-id $terminal

bindsym $mod+d		exec --no-startup-id j4-dmenu-desktop --dmenu='dmenu -fn "Fira Mono-9:medium" -l 3 -i'
