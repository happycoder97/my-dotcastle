#!/bin/bash
source $DOTCASTLE/_bash_helpers/load_helpers
BUILDFILE='./build/home/config.fish'

mkdir -p $(dirname $BUILDFILE)

echo >$BUILDFILE

for a in $DOTCASTLE/common/*.aliases; do
    cat "$a"|
	    sed 's/&&/; and/'| # Fish doesn't support `command1 && command2`
	    prepend_header '# --- '$a' --- '  >>"$BUILDFILE"
done
