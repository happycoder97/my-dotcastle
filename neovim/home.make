#!/bin/bash
source $DOTCASTLE/_bash_helpers/load_helpers
BUILDDIR='./build/home'
mkdir -p $BUILDDIR

BUILDFILE="$BUILDDIR/init.vim"
BUILDFILE_FOR_GUI="$BUILDDIR/ginit.vim"

echo >$BUILDFILE
for i in ./home.include/*.nvim; do
  cat "$i"|envsubst_safe|prepend_header '\" --- '$i' --- '  >>"$BUILDFILE"
done

echo >$BUILDFILE_FOR_GUI
for i in ./home.include/*.gnvim; do
  cat "$i"|envsubst_safe|prepend_header '\" --- '$i' --- '  >>"$BUILDFILE_FOR_GUI"
done

## VIM PLUG
if [ -n "$REDOWNLOAD" ]; then
curl -fLo "$BUILDDIR/plug.vim" \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
else
echo '$UPDATE env var not set; Not redownloading vim plug'
fi

