

This directory contains:

    `environment_vars`
    Contains environment variables definition in a bash compatible format.
    Bash and compatible shell include this file directly to their startup script.
    Fish shell will have to parse this in the `.make` script.

    `*.aliases`
    List of quick aliases for applications like git.
    Bash, Zsh and Fish shares the same syntax for `alias` command.
    Example: `alias gst='git status'`
    So all of these files are directly included in the shell startup scripts.

