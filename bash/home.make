#!/bin/bash
source $DOTCASTLE/_bash_helpers/load_helpers
BUILDFILE='./build/home/bashrc'

mkdir -p $(dirname $BUILDFILE)

echo >$BUILDFILE

for a in $DOTCASTLE/common/*.aliases; do
    cat "$a"|
	    prepend_header '# --- '$a' --- '  >>"$BUILDFILE"
done

for a in home.include/*.bash; do
    cat "$a"|
	    prepend_header '# --- '$a' --- '  >>"$BUILDFILE"
done
