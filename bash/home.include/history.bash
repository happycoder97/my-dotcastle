export HISTSIZE=1000000
export HISTFILESIZE=1000000000

# append instead of overwrite
shopt -s histappend
# append history whenever prompt is displayed
PROMPT_COMMAND='history -a'
# history is now shared between shells. yay! :)
