function prompt() {
    right="\[\e[1;30m\]$?\[\e[0m\]"
    left="\[\e[1;30m\]\w) \[\e[0m\]"
    pad="----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    len="$(($(tput cols)-${#left}-${#right}+44))"

    # PS1=$(printf "%*s\r%s\n\[\e[1;30m\]\$\[\e[0m\] " "$(($(tput cols)+${compensate}))" "asdfg" "$(prompt_left)")
    PS1=$(printf "\[\e[1;37m\]%*.*s\[\e[0m\] %s\r%s\n\[\e[1;37m\]$\[\e[0m\] " 0 $len $pad "$right" "$left")
}
PROMPT_COMMAND=prompt
