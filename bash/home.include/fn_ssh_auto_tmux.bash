ssht() {
    if [ -z "$*" ]; then
        echo "ssht hostname [ssh options]"
        echo "SSH into the hostname and automatically attach to the tmux session"
        return
    fi
    ssh $* -t "tmux a||tmux new"
}
