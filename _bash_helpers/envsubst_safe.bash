
function envsubst_safe() {
    source "$DOTCASTLE/common/environment_vars"
    envsubst $(printenv | awk -F"=" '{printf "$"$1}')
}
