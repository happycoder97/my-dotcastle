
# Bash Helpers
###### for use in `.make` and `.install` scripts written in bash

Source `$DOTCASTLE/_bash_helpers/load_helpers`, which will load all `.bash` files
in `_bash_helpers` directory.
