(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(autoload 'elglot "elglot")
(add-hook 'rust-mode-hook #'flycheck-mode)
