(require 'quick-peek)

(with-eval-after-load 'flycheck
  (flycheck-inline-mode))

;(setq flycheck-inline-display-function
;      (lambda (msg pos)
;        (quick-peek-show msg))
;      flycheck-inline-clear-function #'quick-peek-hide)

(setq flycheck-inline-display-function
      (lambda (msg pos)
        (let* ((ov (quick-peek-overlay-ensure-at pos))
               (contents (quick-peek-overlay-contents ov)))
          (setf (quick-peek-overlay-contents ov)
                (concat contents (when contents "\n") msg))
          (quick-peek-update ov)))
      flycheck-inline-clear-function #'quick-peek-hide)
