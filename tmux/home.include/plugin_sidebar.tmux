set -g @plugin 'tmux-plugins/tmux-sidebar'

set -g @sidebar-tree-command 'tree -C -L 3'
set -g @sidebar-tree-pager 'less -R'

